require 'sinatra'
require 'json'
require 'sinatra/content_for'
require_relative 'models/cart'
require_relative 'models/store'
require_relative 'models/service_provider'
require_relative 'models/order'

configure do
  enable :sessions
  set :session_secret, 'b0sco'
  disable :protection
  # set :static_cache_control, [:public, :max_age => 3600]
end

before do
  # expires 3600, :public, :must_revalidate
end

get '/' do
  @page = 'home'
  @swipe_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :index, :layout => :layout
end

get '/promo/:id' do
  erb "landing_page_#{params[:id]}".to_sym, :layout => :layout
end

get '/products' do
  @page = 'products'
  @cart_items = Cart.total_items(session[:cart])
  erb :products_browse, :layout => :layout
end

get '/products/list' do
  @page = 'products'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :products_list, :layout => :layout
end

get '/products/detail' do
  @page = 'products'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :products_detail, :layout => :layout
end

get '/products/reviews' do
  @page = 'products'
  @cart_items = Cart.total_items(session[:cart])
  erb :products_reviews, :layout => :layout
end

get '/products/:category' do
  @page = 'products'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  @category = params[:category]
  cat_hash = {'lawn-care' => 'Lawn Care', 'gardening' => 'Gardening ', 'trees-leaves' => 'Trees &amp; Leaves', 'snow' => 'Snow', 'power-products' => 'Power Products'}
  @category_title = cat_hash[@category]
  erb :products_browse_category, :layout => :layout
end

get '/stores' do
  @cart_items = Cart.total_items(session[:cart])
  @page = 'store'
  @lat = (params[:latitude] if !params[:latitude].nil? && params[:latitude] != "") || false
  @lon = (params[:longitude] if !params[:longitude].nil? && params[:longitude] != "") || false
  @zip = (params[:zip] if !params[:zip].nil? && params[:zip] != "") || false
  @radius = (params[:radius] if !params[:radius].nil? && params[:radius] != "") || false
  @results = (true if @radius && (@zip || (@lat && @lon))) || false
  @stores = Store.stores
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @current_location = @lat && @lon || false
  erb :stores, :layout => :layout
end

get '/stores/directions/:id' do
  @cart_items = Cart.total_items(session[:cart])
  @page = 'store'
  @zepto_js = 'yes'
  @map_js = 'yes'
  @troy_js = 'yes'
  @id = params[:id]
  @start = params[:address]
  @radius = params[:radius]
  @store = Store.stores.select { |s| s[:id] === Integer(params[:id]) }[0]
  erb :stores_directions, :layout => :layout
end

get '/stores/map/:id' do
  @cart_items = Cart.total_items(session[:cart])
  @page = 'store'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @lat = params[:latitude]
  @lon = params[:longitude]
  @zip = params[:zip]
  @radius = params[:radius]
  @store = Store.stores.select { |s| s[:id] === Integer(params[:id]) }[0]
  erb :stores_map, :layout => :layout
end

get '/cart' do
  @page = 'cart'
  @cart = session[:cart]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
  erb :cart, :layout => :layout
end

get '/cart/add/error' do
  @page = 'cart'
  @cart = session[:cart]
  @cart_items = Cart.total_items(session[:cart])
  erb :cart_add_error, :layout => :layout
end

post '/cart/update' do
  @page = 'cart'
  session[:cart] = Cart.update_item(
                          session[:cart], 
                          params[:product_id],
                          params[:product_quantity]
                        )
  redirect to('/cart')
end

post '/cart/delete' do
  @page = 'cart'
  session[:cart] = Cart.delete_item(
                          session[:cart], 
                          params[:product_id]
                        )
  redirect to('/cart')
end

post '/cart' do
  random = rand(9)
  if random != 0
    @page = 'cart'
    session[:cart] = Cart.add_item(
                            session[:cart], 
                            params[:product_id],
                            params[:product_price], 
                            params[:product_quantity]
                          )
    redirect to('/cart')
  else
    redirect to ('/cart/add/error')
  end
  
end

post '/cart/promo' do
  @page = 'cart'
  @cart = session[:cart]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @error = true
  @cart_items = Cart.total_items(session[:cart])
  erb :cart, :layout => :layout
end

get '/cart/checkout' do
  if request.cookies["logged-in"] == 'true'
    redirect to '/cart/checkout/shipping'
  end
  @page = 'cart'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart = session[:cart]
  @cart_items = Cart.total_items(session[:cart])
  erb :cart_sign_in, :layout => :layout
end

post '/cart/checkout/signin' do
  redirect to('/cart/checkout/shipping')
end

get '/cart/checkout/shipping' do
  @page = 'cart'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart = session[:cart]  
  @cart_items = Cart.total_items(session[:cart])
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
  erb :cart_shipping, :layout => :layout
end

post '/cart/checkout/shipping' do
  redirect to('/cart/checkout/payment')
end

get '/cart/checkout/payment' do
  if request.cookies["logged-in"] == 'false'
    redirect to '/cart/checkout'
  end
  @page = 'cart'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart = session[:cart]  
  @cart_items = Cart.total_items(session[:cart])
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
  erb :cart_payment, :layout => :layout
end

post '/cart/checkout/payment' do
  redirect to('/cart/checkout/thanks')
end

get '/cart/checkout/thanks' do
  @page = 'cart'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart = session[:cart] 
  @cart_items = Cart.total_items(session[:cart])
  @cart_amount = Cart.total_amount(session[:cart])
  @cart_subamount = @cart_amount - 3
  session[:cart] = nil  
  erb :cart_thanks, :layout => :layout
end

get '/videos' do
  @page = 'videos'
  @cart_items = Cart.total_items(session[:cart])
  erb :videos_watch, :layout => :layout
end

get '/videos/:category' do
  @page = 'videos'
  @cart_items = Cart.total_items(session[:cart])
  @category = params[:category]
  cat_hash = {'maintenance' => 'Maintenance', 'repairs' => 'Repairs', 'tips-tricks' => 'Tips &amp; Tricks', 'gardening-videos' => 'Gardening Videos'}
  @category_title = cat_hash[@category]
  erb :videos_watch_category, :layout => :layout
end

get '/forgot-username' do
  @page = 'sign-in'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @from = params[:from]
  @cart_items = Cart.total_items(session[:cart])
  erb :forgot_username, :layout => :layout
end

post '/forgot-username' do
  if params[:email]
    @flash_notice = "Your username has been emailed to #{params[:email]}"
  end
  @page = 'sign-in'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :forgot_username, :layout => :layout
end

get '/forgot-password' do
  @page = 'sign-in'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @from = params[:from]
  @cart_items = Cart.total_items(session[:cart])
  erb :forgot_password, :layout => :layout
end

post '/forgot-password' do
  if params[:email]
    @flash_notice = "Your username has been emailed to #{params[:email]}"
  end
  @page = 'sign-in'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :forgot_password, :layout => :layout
end

get '/change-password' do
  @page = 'change-password'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :change_password, :layout => :layout
end

post '/change-password' do
  @page = 'change-password-success'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :change_password_success, :layout => :layout
end

get '/sign-in' do
  @page = 'sign-in'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :sign_in, :layout => :layout
end

get '/search' do
  @page = 'search'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :search_results, :layout => :layout
end

get '/orders' do
  if request.cookies["logged-in"] == 'true'
    redirect to '/orders/list'
  end
  @page = 'orders'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :order_status, :layout => :layout
end

get '/orders/details' do
  if request.cookies["logged-in"] == 'true'
    redirect to '/orders/list'
  end
  @page = 'orders'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :order_details, :layout => :layout
end

get '/orders/view-details' do
  @page = 'orders'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  if params[:order_number].nil?
    @active_order = Order.get_order(0)
  else
    @active_order = Order.get_order(params[:order_number])
  end
  @cart_items = Cart.total_items(session[:cart])
  erb :order_view_details, :layout => :layout
end

get '/orders/list' do
  if !request.cookies["logged-in"] || request.cookies["logged-in"] == 'false'
    redirect to '/orders'
  end
  @page = 'orders'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @order_numbers = Order.orders
  @cart_items = Cart.total_items(session[:cart])
  erb :order_status_results, :layout => :layout
end

get '/orders/tracking' do
  if params[:order_number].nil?
    redirect to '/orders/list'
  end
  @page = 'orders'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :order_tracking, :layout => :layout
end

get '/owners' do
  @page = 'owners'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :owners_center, :layout => :layout
end

get '/owners/:category' do
  @page = 'owners'
  @category = params[:category]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :owners_center_category, :layout => :layout
end

get '/owners/manuals/decline-terms' do
  redirect to '/owners/manuals'
end

get '/owners/manuals/results' do
  @page = 'owners'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :owners_center_manuals, :layout => :layout
end

post '/owners/manuals/results' do
  @page = 'owners'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :owners_center_manuals, :layout => :layout
end

post '/owners/find-parts/results' do
  @page = 'owners'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @part_number = params[:part_number]
  @cart_items = Cart.total_items(session[:cart])
  erb :owners_center_parts_results, :layout => :layout
end

get '/owners/manuals/:id' do
  @page = 'owners'
  @id = params[:id]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  if request.cookies["manuals-accepted-terms"] == 'true'
    erb :owners_center_manuals_view, :layout => :layout
  else
    erb :owners_center_manuals_accept, :layout => :layout
  end  
end

get '/owners/service-locator/results' do
  @page = 'owners'
  @cart_items = Cart.total_items(session[:cart])
  @lat = params[:latitude]
  @lon = params[:longitude]
  @zip = params[:zip]
  @radius = params[:radius]
  @service_providers = ServiceProvider.service_providers
  erb :owners_center_service_results, :layout => :layout
end

get '/owners/service-locator/map/:id' do
  @cart_items = Cart.total_items(session[:cart])
  @page = 'store'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @lat = params[:latitude]
  @lon = params[:longitude]
  @zip = params[:zip]
  @radius = params[:radius]
  @service_provider = ServiceProvider.service_providers.select { |s| s[:id] === Integer(params[:id]) }[0]
  erb :owners_center_service_map, :layout => :layout
end

get '/tips/article-a' do
  @page = 'tips'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  @doc_title = URI.escape('Leaf Cleanup Tips and Leaf Removal Equipment from Troy-Bilt')
  @doc_uri = '/tips/article-a'
  @email_success = params[:email] || false
  erb :tips_advice_article_a, :layout => :layout  
end

get '/email-friend' do
  @page = 'email-friend'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @doc_title = params[:title]
  @doc_uri = params[:uri]
  erb :email_friend_form, :layout => :layout
end

post '/email-friend' do
  redirect "#{params[:doc_uri]}?email=y"
end

get '/tips/article-b' do
  @page = 'tips'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  @doc_title = URI.escape('Leaf Cleanup Tips and Leaf Removal Equipment from Troy-Bilt')
  @doc_uri = '/tips/article-a'
  erb :tips_advice_article_b, :layout => :layout  
end

get '/tips/video' do
  @page = 'tips'
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :tips_advice_video, :layout => :layout  
end

get '/owners/how-to-articles/video-step-by-step' do
  @page = 'owners'
  @id = params[:id]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :how_to_video_steps, :layout => :layout  
end

get '/owners/how-to-articles/article-step-by-step' do
  @page = 'owners'
  @id = params[:id]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :how_to_article_steps, :layout => :layout  
end

get '/owners/how-to-articles/article' do
  @page = 'owners'
  @id = params[:id]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :how_to_article, :layout => :layout  
end

get '/owners/how-to-articles/article-table' do
  @page = 'owners'
  @id = params[:id]
  @zepto_js = 'yes'
  @troy_js = 'yes'
  @cart_items = Cart.total_items(session[:cart])
  erb :how_to_article_table, :layout => :layout  
end

get '/contact-us' do
  @page = 'contact'
  @cart_items = Cart.total_items(session[:cart])
  erb :contact_us, :layout => :layout
end

get '/privacy-policy' do
  @page = 'privacy'
  @cart_items = Cart.total_items(session[:cart])
  erb :privacy_policy, :layout => :layout
end

get '/locate-model' do
  @page = 'locate-model'
  @cart_items = Cart.total_items(session[:cart])
  erb :locate_model, :layout => :layout
end

get '/education/:category' do
  @page = 'education-category'
  @category = params[:category]
  @cart_items = Cart.total_items(session[:cart])
  erb :education_category, :layout => :layout
end

get '/education/:category/:subcategory' do
  @page = 'education-category'
  @category = params[:category]
  @subcategory = params[:subcategory]
  @cart_items = Cart.total_items(session[:cart])
  erb :education_category, :layout => :layout
end

#login and logout controllers
post '/login' do
  response.set_cookie("logged-in", true)
  if params[:return_url] && params[:return_url] != ''
    redirect to params[:return_url]
  elsif request.referer && request.url != request.referer
    redirect to request.referer
  else
    redirect to '/'
  end
end

get '/logout' do
  request.cookies["logged-in"] = nil
  response.set_cookie("logged-in", false)
  if request.referer && request.url != request.referer
    redirect to request.referer
  else
    redirect to '/'
  end
end
