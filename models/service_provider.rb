class ServiceProvider
  
  def self.service_providers
    @service_providers = [
      { :id => 1, :name => "VAN AKEN HARDWARE, INC.", :address => "20159 VAN AKEN BLVD.", :city => "SHAKER HEIGHTS", :state => "OH", :zip => "44122", :phone => "(216) 752-2288" },
      { :id => 2, :name => "GAMBINO POWER EQUIPMENT", :address => "16143 BROADWAY AVENUE", :city => "MAPLE HEIGHTS", :state => "OH", :zip => "44137", :phone => "(216) 662-6630" },
      { :id => 3, :name => "LARRY'S GARDEN CENTER", :address => "17388 BROADWAY AVENUE", :city => "MAPLE HEIGHTS", :state => "OH", :zip => "44137", :phone => "(216) 662-3884" }
    ]
  end
  
end