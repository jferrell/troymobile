class Store
  
  def self.stores
    @stores = [
      { 
        :id => 1,
        :name => "Lowe's Bedford Heights",
        :street => "24500 Miles Road",
        :address => "24500 Miles Road",
        :city => "Bedford Heights",
        :state => "OH",
        :zip => "44146",
        :phone => "(216) 831-2860",
        :lat => 41.422025,
        :long => -81.507091
      },
      { 
        :id => 2,
        :name => "Van Aken Hardware Inc.",
        :street => "21059 Van Aken Blvd.",
        :address => "21059 Van Aken Blvd.",
        :city => "Shaker Heights",
        :state => "OH",
        :zip => "44122",
        :phone => "(216) 752-2288",
        :lat => 41.465242,
        :long => -81.536701
      },
      { 
        :id => 3,
        :name => "Lowe's Macedonia",
        :street => "8224 Golden Link Boulevard",
        :address => "8224 Golden Link Boulevard",
        :city => "Macedonia",
        :state => "OH",
        :zip => "44067",
        :phone => "(330) 908-2750",
        :lat => 41.311781,
        :long => -81.525009
      }
    ]
  end
  
end