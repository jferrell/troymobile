class Cart
  
  def self.get_product_info(id)
    @products = { '1419369' => { 'name' => 'TB130 TriAction&reg; High-Wheel Walk-Behind Push Mower', 'photo' => 'prod-list-1.png', 'backordered' => 1 },
                  '1558828' => { 'name' => 'TB R16 16&quot; Walk-Behind Reel Mower', 'photo' => 'prod-list-2.png', 'backordered' => 0 },
                  '1558830' => { 'name' => 'TB R18 18&quot; Reel Mower', 'photo' => 'prod-list-3.png', 'backordered' => 0 },
                  '1410819' => { 'name' => 'TB110 TriAction&reg; 21&quot; Walk-Behind Push Mower', 'photo' => 'prod-list-4.png', 'backordered' => 0 },
                  '1419371' => { 'name' => 'TB210 TriAction&reg; 21&quot; Front Wheel Drive Self-Propelled Mower', 'photo' => 'prod-list-5.png', 'backordered' => 1 },
                }
    @products[id]
  end
    
  def self.add_item(cart, id, price, quantity)
    @product = self.get_product_info(id)
    item = { id => { 'product_name' => @product['name'], 'product_price' => price, 'product_quantity' => quantity, 'product_photo' => @product['photo'], 'product_backordered' => @product['backordered'] }}
    if cart
      self.update_item_qty(cart, id, quantity, item)
    else
      item
    end
  end
  
  def self.update_item(cart, id, quantity)
    if quantity.to_i <= 0
      cart.delete(id)
    else
      cart[id]['product_quantity'] = quantity.to_s
    end
    cart
  end
  
  def self.delete_item(cart, id)
    cart.delete(id)
    if cart.size > 0
      cart
    else
      cart = nil
    end
  end
  
  def self.update_item_qty(cart, id, new_qty, item)
    if cart
      if cart.has_key?(id)
        qty = cart[id]['product_quantity'].to_i + new_qty.to_i
        cart[id]['product_quantity'] = qty.to_s
        cart
      else
        cart.merge(item)
      end
    end
  end
  
  def self.total_items(cart)
    total = 0
    if cart
      cart.each do |k,v|
        total = total + v['product_quantity'].to_i
      end
    end
    total
  end
  
  def self.total_amount(cart)
    amount = 0
    if cart
      cart.each do |k,v|
        amount = amount + (v['product_quantity'].to_i * v['product_price'].to_f)
      end
    end
    amount.round(2)
  end
  
end